# Motor driver with position feedback HAT

The aim of the project is to design a motor driver with position feedback HAT that can be attached to the 40 GPIO pins of the Pi. The HAT will make it possible to track and control the instantaneous position of the M1N10FB11G Brushless DC Motor which has an incremental encoder attached to its shaft.
